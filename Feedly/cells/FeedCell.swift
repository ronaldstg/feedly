//
//  FeedCell.swift
//  Feedly
//
//  Created by Ronald de Souza on 17/01/18.
//  Copyright © 2018 Ronald de Souza. All rights reserved.
//

import SnapKit
import SwipeCellKit

class FeedCell : SwipeTableViewCell{
    
    //Do whatever you want as exta customization
    var itemLabel = UILabel()
    var isSaved:Bool!
    let heartImageView:UIImageView = UIImageView()

    override func layoutSubviews() {
        
        addSubview(heartImageView)
        addSubview(itemLabel)
        
        if (isSaved) {
            heartImageView.image = UIImage(named:"heart_filled")
        } else {
            heartImageView.image = UIImage(named:"heart_clear")
        }
        
        heartImageView.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.left.equalTo(10)
            make.width.equalTo(25)
            make.height.equalTo(25)
        }
        
        itemLabel.font = UIFont.boldSystemFont(ofSize: 18)
        itemLabel.textColor = UIColor.black
        itemLabel.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(heartImageView.snp.right).offset(5)
            make.right.equalTo(-20)
            make.height.equalTo(22)
        }
    }
}

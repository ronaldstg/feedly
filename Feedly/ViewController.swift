//
//  ViewController.swift
//  Feedly
//
//  Created by Ronald de Souza on 17/01/18.
//  Copyright © 2018 Ronald de Souza. All rights reserved.
//

import UIKit
import SnapKit
import SWXMLHash
import SwipeCellKit
import CoreData

class ViewController: UIViewController, SwipeTableViewCellDelegate {
 
    var navBar: UINavigationBar!
    var apiService: ApiService!
    var loading:CircularLoading!
    
    var tableView = UITableView()
    var feedTitle:[String] = [""]
    var feedDescription:[String] = [""]
    
    var feedItems: [NSManagedObject] = []
    var selectedIndex:Int!
  
    struct Feed {
        var isSaved:Bool
        var title: String
        var description: String
    }
    var feedData:[Feed] = []
    
    override func viewWillAppear(_ animated: Bool) {
        apiService = ApiService.init(apiBaseUrl: "https://www.macworld.co.uk/news/iphone/rss")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loading = CircularLoading.init(view: view)
       
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.loading.start()
        self.apiService.getFeed(completion: feedCallback)
        placeNavBar()
        placeTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func placeNavBar() {
        
        UIApplication.shared.isStatusBarHidden = true
        
        navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        self.view.addSubview(navBar)
        let navItem = UINavigationItem(title: "FEEDLY ITLEAN");
        
        let btnFavorites = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFavorites.layer.masksToBounds = true
        btnFavorites.setImage(UIImage(named: "save_icon"), for: .normal)
        btnFavorites.imageEdgeInsets = UIEdgeInsetsMake(5, 10, 5, 5)
        btnFavorites.addTarget(self, action: #selector(pressButton(button:)), for: .touchUpInside)
        
        let exitItem = UIBarButtonItem(title: "Sair", style: .plain, target: self, action: #selector(exitTapped))
        exitItem.tintColor = .black
        navItem.leftBarButtonItem = exitItem
        
        let rightBarButton = UIBarButtonItem(customView: btnFavorites)
        navItem.rightBarButtonItem = rightBarButton
        navBar.setItems([navItem], animated: false);
    }
    
    func placeTableView() {
        
        tableView.frame = CGRect(x: 0, y: self.navBar.frame.height, width: view.frame.width, height: view.frame.height - self.navBar.frame.height)
        tableView.delegate   = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 45
        
        tableView.register(FeedCell.self as AnyClass, forCellReuseIdentifier: "Cell")
        
        self.view.addSubview(tableView)
    }
    
    func feedCallback(response: XMLIndexer?, status: Int?) -> () {
        self.loading.stop()
        self.feedTitle.removeAll()
        self.feedDescription.removeAll()
        
        for elem in response!["rss"]["channel"]["item"].all {
           
            self.feedData.append(Feed(isSaved: false, title: elem["title"].element!.text, description: elem["description"].element!.text))
            
        }
        
        
        self.tableView.reloadData()
    }
    
    func save(title: String, detail:String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "FeedItem",
                                                in: managedContext)!
        
        let feedItem = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        feedItem.setValue(title, forKeyPath: "title")
        feedItem.setValue(detail, forKeyPath: "detail")
        
        do {
            try managedContext.save()
            feedItems.append(feedItem)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    @objc func exitTapped(sender: UIBarButtonItem) {
        
        let alertController = UIAlertController(title: "Deseja sair", message: "Deseja sair do Feedly?", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Sair", style: UIAlertActionStyle.default) {
            UIAlertAction in
            exit(0);
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "mainToShowFeedSegue" {
            if let showFeedViewController = segue.destination as? ShowFeedViewController {
                showFeedViewController.feedData = self.feedData[self.selectedIndex]
            }
        } else {
            print("not segue identified")
        }
    }
    
    @objc func pressButton(button: UIButton) {
        self.performSegue(withIdentifier: "mainToSavedFeedSegue", sender: self)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

// MARK: - UITableViewDataSource
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feedData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FeedCell
        let index = indexPath.row as Int
        
        cell.delegate = self
        cell.itemLabel.text = self.feedData[index].title
        cell.isSaved = self.feedData[index].isSaved
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let index = indexPath.row as Int
        self.selectedIndex = index
        
        self.performSegue(withIdentifier: "mainToShowFeedSegue", sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard orientation == .right else { return nil }
        let index = indexPath.row as Int
        
        let saveAction = SwipeAction(style: .default, title: "Salvar") { action, indexPath in
            
            self.save(title: self.feedData[index].title, detail: self.feedData[index].description)
            self.feedData[index].isSaved = true
            self.tableView.reloadData()
            
        }
        
        return [saveAction]
    }
    
}


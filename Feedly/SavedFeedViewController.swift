//
//  SavedFeedViewController.swift
//  Feedly
//
//  Created by Ronald de Souza on 17/01/18.
//  Copyright © 2018 Ronald de Souza. All rights reserved.
//

import UIKit
import CoreData
import SwipeCellKit

class SavedFeedViewController: UIViewController {

    var navBar: UINavigationBar!
    var tableView = UITableView()
    var feedItems: [NSManagedObject] = []
    var selectedIndex:Int!
    
    override func viewWillAppear(_ animated: Bool) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FeedItem")
        do {
            feedItems = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        placeNavBar()
        placeTableView()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "savedFeedToShowSegue" {
            if let showFeedViewController = segue.destination as? ShowFeedViewController {
                
                let feedItem = feedItems[self.selectedIndex]
                let title = feedItem.value(forKeyPath: "title") as? String
                let description = feedItem.value(forKeyPath: "detail") as? String
                let selectedData = ViewController.Feed(isSaved: true, title: title!, description: description! )
                
                showFeedViewController.feedData = selectedData
            }
        }
    }
    
    func placeNavBar() {
        navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        self.view.addSubview(navBar);
        let navItem = UINavigationItem(title: "FEEDLY - FAVORITOS");
      
        let backItem = UIBarButtonItem(title: "Voltar", style: .plain, target: self, action: #selector(backTapped))
        backItem.tintColor = .black
        navItem.leftBarButtonItem = backItem
        navBar.setItems([navItem], animated: false);
    }
    
    func placeTableView() {
        
        tableView.frame = CGRect(x: 0, y: self.navBar.frame.height, width: view.frame.width, height: view.frame.height - 100)
        tableView.delegate   = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 45
        
        tableView.register(SwipeTableViewCell.self as AnyClass, forCellReuseIdentifier: "Cell")
        
        self.view.addSubview(tableView)
    }
    
    
    
    @objc func backTapped(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

// MARK: - UITableViewDataSource
extension SavedFeedViewController: UITableViewDelegate, UITableViewDataSource, SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SwipeTableViewCell
        let index = indexPath.row as Int
        let feedItem = feedItems[index]
    
        cell.delegate = self
        cell.textLabel?.text = feedItem.value(forKeyPath: "title") as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let index = indexPath.row as Int
        self.selectedIndex = index
        
        self.performSegue(withIdentifier: "savedFeedToShowSegue", sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard orientation == .right else { return nil }
        let index = indexPath.row as Int
        
        let deleteAction = SwipeAction(style: .destructive, title: "Apagar") { action, indexPath in
            self.deleteRow(itemIndex:index)
        }
        
        return [deleteAction]
    }
    
    func deleteRow(itemIndex:Int) {
        
       guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let objectToDelete = self.feedItems[itemIndex]
        managedContext.delete(objectToDelete)
        
        self.feedItems.remove(at: itemIndex)
        self.tableView.reloadData()
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
}




//
//  APIService.swift
//  feedpoc
//
//  Created by Ronald de Souza on 17/01/18.
//  Copyright © 2018 Ronald de Souza. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash

public class ApiService {
    
    var apiBaseUrl: String = ""
    
    internal init(apiBaseUrl: String) {
        self.apiBaseUrl = apiBaseUrl
    }
    
    internal func getFeed(completion: @escaping (XMLIndexer, Int?) -> Void) {
        
        let url = self.apiBaseUrl
  
        Alamofire.request(url, method: .get, encoding: URLEncoding.default)
            .responseString { response in
                
                let statusCode = response.response?.statusCode
                
                if let string = response.result.value {
                    
                    let xml = SWXMLHash.parse(string)
                    completion(xml,statusCode)
                }
        }
    }
}


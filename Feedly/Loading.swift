//
//  Loading.swift
//  Feedly
//
//  Created by Ronald de Souza on 17/01/18.
//  Copyright © 2017 Ronald de Souza. All rights reserved.
//

import UIKit
import JTMaterialSpinner

class CircularLoading{
    
    let loadingView = UIView()
    var spinnerView = JTMaterialSpinner()
    var view:UIView!
    
    init(view: UIView){
        
        self.view = view
        
        loadingView.frame = view.bounds
        loadingView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.50)
        loadingView.layer.zPosition = 100
        
        loadingView.tag = 10
        loadingView.addSubview(spinnerView)
        
        spinnerView.snp.makeConstraints { (make) in
            make.height.equalTo(80)
            make.width.equalTo(80)
            make.center.equalTo(loadingView)
        }
        
    }
    
    func start() {
        view.addSubview(loadingView)
        view.bringSubview(toFront: loadingView)
        
        spinnerView.beginRefreshing()
    }
    
    func stop()
    {
        spinnerView.endRefreshing()
        if let viewWithTag = self.view.viewWithTag(10) {
            viewWithTag.removeFromSuperview()
        }
    }
}



//
//  ShowFeedViewController.swift
//  Feedly
//
//  Created by Ronald de Souza on 17/01/18.
//  Copyright © 2018 Ronald de Souza. All rights reserved.
//

import UIKit

class ShowFeedViewController: UIViewController {
    
    var navBar: UINavigationBar!
    var tableView = UITableView()
    var titleLabel = UILabel()
    var descriptionLabel = UILabel()
    var URL = UILabel()
    
    var feedData:ViewController.Feed!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        placeNavBar()
        placeDetails()
    }
    
    func placeNavBar() {
        //UIApplication.shared.isStatusBarHidden = true
        
        navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        self.view.addSubview(navBar)
        let navItem = UINavigationItem(title: "FEEDLY ITLEAN");
        
        let btnFavorites = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFavorites.layer.masksToBounds = true
        btnFavorites.setImage(UIImage(named: "save_icon"), for: .normal)
        btnFavorites.imageEdgeInsets = UIEdgeInsetsMake(5, 10, 5, 5)
        btnFavorites.addTarget(self, action: #selector(pressButton(button:)), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem(customView: btnFavorites)
        
        
        let backItem = UIBarButtonItem(title: "Voltar", style: .plain, target: self, action: #selector(backTapped))
        backItem.tintColor = .black
        navItem.leftBarButtonItem = backItem
        navItem.rightBarButtonItem = rightBarButton
        navBar.setItems([navItem], animated: false);
    }
    
    func placeDetails() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        
        titleLabel.text = self.feedData.title
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = UIFont.boldSystemFont(ofSize: 24)
        titleLabel.sizeToFit()
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.navBar.frame.height + 30)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(30)
        }
        
        descriptionLabel.text = self.feedData.description
        descriptionLabel.font = UIFont.systemFont(ofSize: 20)
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = 0
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.sizeToFit()
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(20)
            make.left.equalTo(20)
            make.right.equalTo(-20)
        }
    }
    
    @objc func backTapped(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func pressButton(button: UIButton) {
        self.performSegue(withIdentifier: "showFeedToSavedFeedSegue", sender: self)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
